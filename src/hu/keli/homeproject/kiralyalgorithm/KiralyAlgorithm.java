package hu.keli.homeproject.kiralyalgorithm;

import hu.keli.homeproject.kiralyalgorithm.participants.Man;
import hu.keli.homeproject.kiralyalgorithm.participants.Woman;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class KiralyAlgorithm {

    private static final String FILENAME = "input-6.txt";

    public static void main(String[] args) {

        String fileName;
        if (args.length == 1) {
            fileName = args[0];
        } else {
            fileName = FILENAME;
        }

        System.out.println("--- Start initializing ---\n");
        List<Man> men = new ArrayList<>();
        List<Woman> women = new ArrayList<>();
        Algorithm.initPreferenceLists(fileName, men, women, true);

        int instanceSize = men.size();

        System.out.println("--- Start Kiraly-algorithm ---\n");
        List<Pair<Man, Woman>> matching = Algorithm.doAlgorithm(men, false);

        System.out.println("Matching size: " + matching.size());
        System.out.println("Matching:" + "\n" + matching);

        System.out.println();
        StabilityTest.matchingTest(matching, true);

        if (instanceSize < 11) {
            System.out.println();
            System.out.println("--- Generate all matchings ---\n");

            int stableMatchingsCount = 0;
            int maxMatchingSize = 0;

            List<Man> testMen = new ArrayList<>();
            List<Woman> testWomen = new ArrayList<>();

            Algorithm.initPreferenceLists(fileName, testMen, testWomen, false);

            System.out.println("Generate permutations of women\n");
            List<List<Woman>> womenPermutations = SmtiGenerator.generatePermutations(testWomen);

            for (List<Woman> womanList : womenPermutations) {

                List<Pair<Man, Woman>> tempMatching = StabilityTest.createMatchings(testMen, womanList, false);

                StabilityTest.deleteInconsistence(tempMatching, false);

                if (StabilityTest.matchingTest(tempMatching, false)) {
                    stableMatchingsCount++;
                    int currentMatchingSize = tempMatching.size();
                    if (maxMatchingSize < currentMatchingSize) {
                        maxMatchingSize = currentMatchingSize;
                    }
                }
            }

            System.out.println("All matchings: " + factorial(instanceSize));
            System.out.println("Stable matchings: " + stableMatchingsCount);
            System.out.println("Max matching size: " + maxMatchingSize);
        }
    }

    private static int factorial(int n) {
        int fact = 1;
        for (int i = 2; i <= n; i++) {
            fact = fact * i;
        }
        return fact;
    }
}

package hu.keli.homeproject.kiralyalgorithm;

import hu.keli.homeproject.kiralyalgorithm.participants.Man;
import hu.keli.homeproject.kiralyalgorithm.participants.Woman;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class StabilityTest {

    public static boolean matchingTest(List<Pair<Man, Woman>> matching, boolean log) {
        for (Pair<Man, Woman> pair : matching) {
            Man man = pair.getKey();
            Integer rankOfPair = man.rank(man.getPair());
            if (rankOfPair != 0) {
                rankOfPair--;
                while (!(rankOfPair < 0)) {
                    for (Woman woman : man.getOriginalPreferenceList().get(rankOfPair)) {
                        if (woman.prefersTo(man, woman.getPair())) {
                            if (log) System.out.println("Instabil matching");
                            if (log) System.out.println("  blocking pair: (" + man + "," + woman + ")");
                            return false;
                        }
                    }
                    rankOfPair--;
                }
            }
        }
        if (log) System.out.println("Stabil matching");
        return true;
    }

    public static List<Pair<Man, Woman>> createMatchings(List<Man> testMen, List<Woman> womanList, boolean log) {
        if (log) System.out.println(womanList);

        List<Pair<Man, Woman>> tempMatching = new ArrayList<>();

        for (int i=0; i<womanList.size(); i++) {
            Man man =  testMen.get(i);
            Woman woman = womanList.get(i);
            man.setPair(woman);
            woman.setPair(man);
            tempMatching.add(new Pair<>(man, woman));
        }

        return tempMatching;
    }

    public static void deleteInconsistence(List<Pair<Man, Woman>> matching, boolean log) {
        if (log) System.out.println("Deleting inconsistencies from matching");
        if (log) System.out.println("  matching: " + matching);

        List<Pair<Man, Woman>> delete = new ArrayList<>();

        for (Pair<Man, Woman> pair : matching) {
            Man man = pair.getKey();
            Woman woman = pair.getValue();

            if (man.rank(woman) == null || !woman.hasAvailablePair()) {
                if (log) System.out.println("Not available pair: (" + man + "," + man.getPair() + ")");
                delete.add(pair);
            }
        }
        matching.removeAll(delete);
    }
}

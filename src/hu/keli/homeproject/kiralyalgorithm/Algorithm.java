package hu.keli.homeproject.kiralyalgorithm;

import hu.keli.homeproject.kiralyalgorithm.participants.Man;
import hu.keli.homeproject.kiralyalgorithm.participants.Participant;
import hu.keli.homeproject.kiralyalgorithm.participants.Woman;

import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Algorithm {

    public static void initPreferenceLists(String fileName, List<Man> men, List<Woman> women, boolean log) {
        try {
            File file = new File("/Users/keli/Documents/IdeaProjects/kiralyalgorithm/src/hu/keli/homeproject/kiralyalgorithm/input/" + fileName);

            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

            int instanceSize = Integer.parseInt(bufferedReader.readLine());

            for (int i=0; i<instanceSize; i++) {
                men.add(new Man(i));
                women.add(new Woman(i));
            }

            System.out.println("Init men's lists");
            initParticipantLists(bufferedReader, men, women);

            System.out.println("Init women's lists");
            initParticipantLists(bufferedReader, women, men);

            bufferedReader.close();
        } catch (Exception ex) {
            System.out.println("File reading error" + ex);
        }
        System.out.println();

        if (log) {
            System.out.println("Men's preference lists:");
            for (Man man : men) {
                System.out.println(man.toStringWithPreferenceList());
            }
            System.out.println("\nWomen's preference lists:");
            for (Woman woman: women) {
                System.out.println(woman.toStringWithPreferenceList());
            }
            System.out.println();
        }
    }

    public static List<Pair<Man, Woman>> doAlgorithm(List<Man> men, boolean log) {

        List<Pair<Man, Woman>> matching = new ArrayList<>();

        System.out.println("Algorithm input size: " + men.size() + "\n");

        int round = 1;
        if (log) System.out.println("-- ROUND " + round);
        Man man = findMan(men, log);
        while (man != null) {
            Woman woman = man.getMostPreferred();

            if (woman != null) {
                if (log) System.out.println("Most preffered: " + woman);

                if (woman.isUnassigned()) {
                    if (log) System.out.println("Women " + woman + " is unassigned");
                    matching.add(createPair(man,woman, log));
                } else {
                    Man manCurrent = woman.getPair();
                    if (log) System.out.println("Women " + woman + " is assigned with " + manCurrent);

                    if (woman.prefersTo(man, manCurrent) || woman.isPrecarious()) {
                        reject(matching, woman, manCurrent, log);
                        matching.add(createPair(man,woman, log));
                    } else {
                        reject(matching, woman, man, log);
                    }
                }
            } else {
                man.setExhausted(true);
            }

            round++;
            if (log) System.out.println("-- ROUND " + round);
            man = findMan(men, log);
        }
        if (log) System.out.println();

        matching.sort((p1, p2) -> p1.getKey().compareTo(p2.getKey()));
        return matching;
    }

    private static <T extends Participant<E>, E extends Participant<T>> void initParticipantLists(BufferedReader bufferedReader,
                                                                                                  List<T> men, List<E> women) throws IOException {
        int instanceSize = men.size();

        int i = 0;
        while (i < instanceSize) {
            String line = bufferedReader.readLine();
            char[] chars = line.toCharArray();

            int j=0;
            while (j < chars.length) {
                if (chars[j] == '(') {
                    j++;
                    StringBuilder tie = new StringBuilder();

                    while (chars[j] != ')') {
                        tie.append(chars[j]);
                        j++;
                    }

                    String[] numbers = tie.toString().split(" ");

                    List<E> listWoman = new ArrayList<>();

                    for (String num : numbers) {
                        listWoman.add(women.get(Integer.parseInt(num)));
                    }

                    men.get(i).addToPreferenceList(listWoman);
                } else if (chars[j] != ' ') {
                    StringBuilder number = new StringBuilder();

                    number.append(chars[j]);

                    j++;
                    if (j < chars.length) {
                        while (j<chars.length && chars[j] != ' ') {
                            number.append(chars[j]);
                            j++;
                        }
                    }

                    men.get(i).addToPreferenceList(women.get(Integer.parseInt(number.toString())));
                }
                j++;
            }
            i++;
        }
    }

    private static Man findMan(List<Man> men, boolean log) {
        for (Man man : men) {
            if (man.isUnassigned() && !man.isExhausted()) {
                if (log) System.out.println("Find man: " + man);
                return man;
            }
        }
        if (log) System.out.println("No man find");
        return null;
    }

    private static Pair<Man, Woman> createPair(Man man, Woman woman, boolean log) {
        if (log) System.out.println("Create pair: (" + man + "," + woman + ")");
        man.setPair(woman);
        woman.setPair(man);
        return new Pair<>(man, woman);
    }

    private static void removePair(List<Pair<Man, Woman>> matching, Woman woman, Man man, boolean log) {
        if (log) System.out.println("Remove pair: (" + man + "," + woman + ")");
        if (matching.remove(new Pair<>(man, woman))) {
            man.setPair(null);
//            women.setPair(null);
        }
    }

    private static void reject(List<Pair<Man, Woman>> matching, Woman w, Man m, boolean log) {
        if (log) System.out.println(w + " rejects " + m);
        boolean precarious = w.isPrecarious();
//        matching.remove(new Pair<>(m, w));
        removePair(matching, w, m, log);

        if (!precarious) {
            if (log) System.out.println(w + " is not precarious");
            delete(m, w, log);
            if (m.isPreferenceListEmpty()) {
                if (m.isSecondChance()) {
                    m.setExhausted(true);
                } else {
                    if (log) System.out.println("Recover " + m.toString());
                    m.setSecondChance(true);
                    m.recover();
                }
            }
        }
    }

    private static void delete(Man man, Woman woman, boolean log) {
        if (log) System.out.println("  Delete: (" + man + "," + woman + ")");
        man.deleteFromPreferenceList(woman);
        woman.deleteFromPreferenceList(man);
        if (log) System.out.println("    " + man.toStringWithPreferenceList());
        if (log) System.out.println("    " + woman.toStringWithPreferenceList());
    }
}

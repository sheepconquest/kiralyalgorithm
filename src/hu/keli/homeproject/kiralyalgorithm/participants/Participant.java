package hu.keli.homeproject.kiralyalgorithm.participants;

import java.util.ArrayList;
import java.util.List;

public abstract class Participant<T> {
    private final int id;
    private T pair = null;
    PreferenceList<T> preferenceList = new PreferenceList<>();

    Participant(int id) {
        this.id = id;
    }

//    public abstract boolean prefersTo(T p1, T p2);
    public Boolean prefersTo(T p1, T p2) {
        if (p1.equals(p2)) {
            return false;
        }

        Integer rank1 = rank(p1);
        Integer rank2 = rank(p2);
        if (rank1 == null || rank2 == null) {
            return false;
        }
        if (rank1 < rank2) {
            return true;
        }
        return null;
    }

    public T getPair() {
        return pair;
    }

    public void setPair(T pair) {
        this.pair = pair;
    }

    public List<List<T>> getOriginalPreferenceList() {
        return preferenceList.getOriginalPreferenceList();
    }

    public void addToPreferenceList(T element) {
        List<T> list = new ArrayList<>();
        list.add(element);
        preferenceList.add(list);
    }
    public void addToPreferenceList(List<T> elements) {
        List<T> list = new ArrayList<>(elements);
        preferenceList.add(list);
    }

    public void deleteFromPreferenceList(T participant) {
        preferenceList.delete(participant);
    }

    public Integer rank(T participant) {
        return preferenceList.getRankOf(participant);
    }

    public T getMostPreferred() {
        return getMostPreferredFromTie(preferenceList.getTopTie());
    }

    public boolean isUnassigned() {
        return pair == null;
    }

    public boolean isPreferenceListEmpty() {
        return preferenceList.isEmpty();
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }

    public String toStringWithPreferenceList() {
        return toString() + ": " + preferenceListToString();
    }

    public String preferenceListToString() {
        return preferenceList.toString();
    }

    private T getMostPreferredFromTie(List<T> tie) {
        if (tie == null) {
            return null;
        }
        T mostPreferred = tie.get(0);
        if (tie.size() > 1) {
            for (T participant : tie) {
                if (prefersTo(participant, mostPreferred)) {
                    mostPreferred = participant;
                }
            }
        }
        return mostPreferred;
    }

    public int compareTo(Participant<T> other) {
        return this.id - other.id;
    }
}

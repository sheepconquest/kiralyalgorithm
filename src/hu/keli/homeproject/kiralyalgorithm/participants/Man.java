package hu.keli.homeproject.kiralyalgorithm.participants;

import java.util.List;

public class Man extends Participant<Woman>{

    private boolean secondChance;
    private boolean exhausted;

    public Man(int id) {
        super(id);
        this.secondChance = false;
        this.exhausted = false;
    }

    public boolean isSecondChance() {
        return this.secondChance;
    }

    public void setSecondChance(boolean secondChance) {
        this.secondChance = secondChance;
    }

    public boolean isExhausted() {
        return this.exhausted;
    }

    public void setExhausted(boolean exhausted) {
        this.exhausted = exhausted;
    }

    @Override
    public Boolean prefersTo(Woman w1, Woman w2) {
//        if (w1.equals(w2)) {
//            return false;
//        }
//
//        Integer rank1 = rank(w1);
//        Integer rank2 = rank(w2);
//        if (rank1 == null || rank2 == null) {
//            return false;
//        }
//        if (rank1 < rank2) {
//            return true;
//        }
        Boolean result = super.prefersTo(w1, w2);
        if (result != null) {
            return result;
        }
//        return rank1.equals(rank2) && w1.isUnassigned() && !w2.isUnassigned();
        return rank(w1).equals(rank(w2)) && w1.isUnassigned() && !w2.isUnassigned();
    }

    public void recover() {
        preferenceList.recover();
        for (List<Woman> tie : preferenceList.getPreferenceList()) {
            for (Woman woman : tie) {
                woman.recover(this);
            }
        }
    }

    boolean hasMostPreferredWoman() {
        List<Woman> topTie = preferenceList.getTopTie();
        for (Woman woman : topTie) {
            if (prefersTo(woman, getPair())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "m" + super.toString();
    }
}

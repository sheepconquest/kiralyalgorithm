package hu.keli.homeproject.kiralyalgorithm.participants;

public class Woman extends Participant<Man> {

    public Woman(int id) {
        super(id);
    }

    @Override
    public Boolean prefersTo(Man m1, Man m2) {
//        if (m1.equals(m2)) {
//            return false;
//        }
//
//        Integer rank1 = rank(m1);
//        Integer rank2 = rank(m2);
//        if (rank1 == null || rank2 == null) {
//            return false;
//        }
//        if (rank1 < rank2) {
//            return true;
//        }
//        return rank1.equals(rank2) && m1.isSecondChance() && !m2.isSecondChance();
        Boolean result = super.prefersTo(m1, m2);
        if (result != null) {
            return result;
        }
        return rank(m1).equals(rank(m2)) && m1.isSecondChance() && !m2.isSecondChance();
    }

    public boolean isPrecarious() {
        return getPair().hasMostPreferredWoman();
    }

    public boolean hasAvailablePair() {
        return rank(getPair()) != null;
    }

    void recover(Man man) {
        preferenceList.recover(man);
    }

    @Override
    public String toString() {
        return "w" + super.toString();
    }
}

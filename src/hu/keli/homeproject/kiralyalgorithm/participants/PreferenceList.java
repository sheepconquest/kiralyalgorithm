package hu.keli.homeproject.kiralyalgorithm.participants;

import java.util.ArrayList;
import java.util.List;

public class PreferenceList<T> {
    private List<List<T>> preferenceList = new ArrayList<>();
    private List<List<T>> originalPreferenceList = new ArrayList<>();
    private int size = 0;

    void add(List<T> element) {
        preferenceList.add(element);
        List<T> save = new ArrayList<>(element);
        originalPreferenceList.add(save);
        size += element.size();
    }

    List<List<T>> getPreferenceList() {
        return preferenceList;
    }

    List<List<T>> getOriginalPreferenceList() {
        return originalPreferenceList;
    }

    List<T> getTopTie() {
        for (List<T> tie : preferenceList) {
            if (!tie.isEmpty()) {
                return tie;
            }
        }
        return null;
    }

    Integer getRankOf(T participant) {
        for (List<T> tie : preferenceList) {
            if (tie.contains(participant)) {
                return preferenceList.indexOf(tie);
            }
        }
        return null;
    }

    boolean isEmpty() {
        return size < 1;
    }

    void delete(T participant) {
        for (List<T> tie : preferenceList) {
            if (tie.remove(participant)) {
                size--;
                break;
            }
        }
    }

    void recover() {
        preferenceList = originalPreferenceList;
        size = 0;
        for (List<T> tie : preferenceList) {
            size += tie.size();
        }
    }

    void recover(T participant) {
        for (List<T> tie : originalPreferenceList) {
            if (tie.contains(participant)) {
                int index = originalPreferenceList.indexOf(tie);
                preferenceList.get(index).add(participant);
                size++;
                break;
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        for (List<T> tie : preferenceList) {
            if (!tie.isEmpty()) {
                if (tie.size() > 1) {
                    stringBuilder.append("(");
                    for (T participant : tie) {
                        stringBuilder.append(participant);
                        stringBuilder.append(" ");
                    }
                    stringBuilder.deleteCharAt(stringBuilder.length()-1);
                    stringBuilder.append(")");
                } else {
                    stringBuilder.append(tie.get(0).toString());
                }
                stringBuilder.append(" ");
            }
        }

        if (stringBuilder.length() > 0) {
            stringBuilder.deleteCharAt(stringBuilder.length()-1);
        }
        return stringBuilder.toString();
    }
}

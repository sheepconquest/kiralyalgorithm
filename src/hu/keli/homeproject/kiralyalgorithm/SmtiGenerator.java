package hu.keli.homeproject.kiralyalgorithm;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class SmtiGenerator {

    private static final int INSTANCE_SIZE = 6;
    private static final Double MIN_MULTIPLIER = 0.5;
    private static final Double MAX_MULTIPLIER = 1.0;

    public static void main(String[] args) {

        Integer instanceSize = INSTANCE_SIZE;
        Double minMultilier = MIN_MULTIPLIER;
        Double maxMultilier = MAX_MULTIPLIER;

        if (args.length == 1) {
            instanceSize = Integer.parseInt(args[0]);
        }
        if (args.length == 3) {
            instanceSize = Integer.parseInt(args[0]);
            minMultilier = valueChecker(Double.parseDouble(args[1])) ? Double.parseDouble(args[1]) : MIN_MULTIPLIER;
            maxMultilier = valueChecker(Double.parseDouble(args[2])) ? Double.parseDouble(args[2]) : MAX_MULTIPLIER;
        }

        int minPreferneceListSize = (int) (instanceSize * minMultilier);
        int maxPreferneceListSize = (int) (instanceSize * maxMultilier);

        List<List<Integer>> men = new ArrayList<>();
        List<List<Integer>> women = new ArrayList<>();

        List<Integer> numbers = new ArrayList<>();
        for (int i=0; i<instanceSize; i++) {
            numbers.add(i);
        }

        boolean log = false;
        System.out.println("Men's lists");
        generatePreferenceList(numbers, men, minPreferneceListSize, maxPreferneceListSize, log);

        System.out.println();

        System.out.println("Women's lists");
        generatePreferenceList(numbers, women, minPreferneceListSize, maxPreferneceListSize, log);

        System.out.println();

        System.out.println("Deleting inconsistencies");
        deleteInconsistence(men, women, log);
        if(log) System.out.println();
        deleteInconsistence(women, men, log);

        System.out.println();

        System.out.println("Add ties");
        addTies(men, false);
        addTies(women, false);

        System.out.println();

        String fileName = "input-" + instanceSize + ".txt";
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));

            writer.write(instanceSize.toString());
            writer.newLine();

            writeFile(writer, men);
            System.out.println();
            writeFile(writer, women);

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <E> List<List<E>> generatePermutations(List<E> original) {
        if (original.size() == 0) {
            List<List<E>> result = new ArrayList<List<E>>();
            result.add(new ArrayList<E>());
            return result;
        }
        E firstElement = original.remove(0);
        List<List<E>> returnValue = new ArrayList<List<E>>();
        List<List<E>> permutations = generatePermutations(original);
        for (List<E> smallerPermutated : permutations) {
            for (int index=0; index <= smallerPermutated.size(); index++) {
                List<E> temp = new ArrayList<E>(smallerPermutated);
                temp.add(index, firstElement);
                returnValue.add(temp);
            }
        }
        return returnValue;
    }

    private static boolean valueChecker(Double num) {
        return num >= 0 && num <= 1;
    }

    private static void generatePreferenceList(List<Integer> numbers, List<List<Integer>> participants,
                                                 int minPreferneceListSize, int maxPreferneceListSize, boolean log) {
        int instanceSize = numbers.size();

        for (int i=0; i<instanceSize; i++) {
            Collections.shuffle(numbers);

            Random random = new Random();
            int preferenceListSize = random.ints(minPreferneceListSize, maxPreferneceListSize+1).findFirst().getAsInt();

            List<Integer> preferenceList = new ArrayList<>();
            for (int j=0; j<preferenceListSize; j++) {
                preferenceList.add(numbers.get(j));
            }
            participants.add(preferenceList);
            if (log) System.out.println(participants.get(i));
        }
    }

    private static void writeFile(BufferedWriter writer, List<List<Integer>> participants) throws IOException{
        for (List<Integer> participant : participants) {
            StringBuilder stringBuilder = new StringBuilder();

            for (Integer value : participant) {
                switch (value) {
                    case -1:
                        stringBuilder.append("(");
                        break;
                    case -2:
                        stringBuilder.deleteCharAt(stringBuilder.length()-1);
                        stringBuilder.append(")");
                        stringBuilder.append(" ");
                        break;
                    default:
                        stringBuilder.append(value);
                        stringBuilder.append(" ");
                        break;
                }
            }

            if (stringBuilder.length() > 0) {
                stringBuilder.deleteCharAt(stringBuilder.length()-1);
            }

            System.out.println(stringBuilder.toString());
            writer.write(stringBuilder.toString());
            writer.newLine();
        }
    }

    private static void deleteInconsistence(List<List<Integer>> lists, List<List<Integer>> checklist, boolean log) {
        for (int i=0; i<lists.size(); i++) {
            List<Integer> delete = new ArrayList<>();

            List<Integer> list = lists.get(i);
            for (Integer index : list) {
                if (!checklist.get(index).contains(i)) {
                    delete.add(index);
                }
            }

            list.removeAll(delete);
            if (log) System.out.println(list);
        }
    }

    private static void addTies(List<List<Integer>> participants, boolean log) {
        for (List<Integer> participant : participants) {
            if(log) System.out.println(participant);

            int size = participant.size();
            Random random = new Random();

            if (size > 1 && (random.nextBoolean() || random.nextBoolean())) {
                int tieCount = random.ints(1,size/2+1).findFirst().getAsInt();
                int maxTieSize = tieCount != 0 ? size/tieCount : 0;
//                int maxTieNumberInList = size/maxTieSize;

                if(log) System.out.println("size: " + size );
                if(log) System.out.println("tie count: " + tieCount);
                if(log) System.out.println("max tie size: " + maxTieSize);
//                System.out.println("max tie number in list: " + maxTieNumberInList);

                List<Integer> index = new ArrayList<>();
                for (int i=0; i<size; i++) {
                    index.add(i);
                }
                Collections.shuffle(index);

                List<Integer> positions = new ArrayList<>();
                for (int i=0; i<tieCount; i++) {
                    positions.add(index.get(i));
                }

                positions.sort(Comparator.naturalOrder());
                if(log) System.out.println("positions: " + positions);

                int actualPosition = 0;
                for (int i=0; i<positions.size(); i++) {
                    if (actualPosition < positions.get(i)) {
                        actualPosition = positions.get(i);
                    }
                    if(log) System.out.println("  act: " + actualPosition);

                    int tieSize = random.ints(2,maxTieSize+1).findFirst().getAsInt();
                    int endPosition = actualPosition + tieSize + 1;
                    if(log) System.out.println("  end: " + endPosition);

                    if (endPosition < size+1) {
                        participant.add(actualPosition, -1);
                        participant.add(endPosition, -2);
                    } else if (endPosition <= size+2) {
                        participant.add(actualPosition, -1);
                        participant.add(-2);
                    }

                    actualPosition = endPosition + 1;
                }

                if(log) System.out.println("updated list: " + participant);
            }

        }

    }
}
